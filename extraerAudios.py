import pysftp
from os import listdir, makedirs
from os.path import isfile, join, exists
from shutil import copy
import sys
from sys import argv


a=input("ingrese el año:")
m=input("ingrese el mes:")
d=input("ingrese el dia:")


año=str(a)
mes=str(m)
dia=str(d)



server = "10.172.5.201" 
user = "root"
password = "G0ph4rm4.,2015"
port=22

cnopts = pysftp.CnOpts()
cnopts.hostkeys = None
with pysftp.Connection(host=server, username=user, password=password, port=port, cnopts=cnopts) as sftp:
    print("Connection succesfully stablished ... ")

    # Cambiar a directorio remoto
    sftp.cwd("/var/spool/asterisk/monitor/")
    
    # Obtener estructura del directorio '/var/www/vhosts'
    directory_structure = sftp.listdir_attr()

    # Imprimir información
    #for attr in directory_structure:
       # if (attr.filename[7:11] == año and attr.filename[11:13] == mes):
        #    print(attr.filename, attr)
       

    #Se definen los directorios 
    directorio_inicial  =r"/var/spool/asterisk/monitor/"
    directorio_final  = r"Z:/CALL CENTER/AUDIOS/" 

    #Switch para nombre del mes de la carpeta
    def switch_mes(argument):
        switcher = {
            "01": "01 ENERO",
            "02": "02 FEBRERO",
            "03": "03 MARZO",
            "04": "04 ABRIL",
            "05": "05 MAYO",
            "06": "06 JUNIO",
            "07": "07 JULIO",
            "08": "08 AGOSTO",
            "09": "09 SEPTIEMBRE",
            "10": "10 OCTUBRE",
            "11": "11 NOVIEMBRE",
            "12": "12 DICIEMBRE"
        }
        return switcher.get(argument)

    def switch_laboratorio(argument):
        switcher = {
            "348": "01 ASTRAZENECA",
            "349": "01 ASTRAZENECA",
            "353": "01 ASTRAZENECA",
            "412": "01 ASTRAZENECA",
            "413": "01 ASTRAZENECA",
            "352": "01 ASTRAZENECA",
            "340": "02 NOVARTIS",
            "341": "02 NOVARTIS",
            "342": "02 NOVARTIS",
            "343": "02 NOVARTIS",
            "344": "02 NOVARTIS",
            "312": "03 BAYER",
            "370": "03 BAYER",
            "311": "04 BOEHRINGER INGELHEIM",
            "313": "04 BOEHRINGER INGELHEIM",
            "317": "04 BOEHRINGER INGELHEIM",
            "347": "04 BOEHRINGER INGELHEIM",
            "323": "07 BIOGEN",
            "332": "07 BIOGEN",
            "351": "07 BIOGEN",
            "354": "08 ROCHE",
            "355": "08 ROCHE"

        }
        return switcher.get(argument,"VARIOS")

    #Genera el directorio donde se copiará el audio  
    def moverFichero( nombre ):
        #discrimina si el audio es out / in / g
        if (nombre[0:3] == "OUT" ):
            #Se declaran las variables año, mes, dia y anexo segun cordenada del filename
            directorioAno = nombre[7:11]
            directorioMes = nombre[11:13]
            directorioDia = nombre[13:15]
            directorioAnexo = nombre[3:6]
            
        elif(nombre[0:2] == "IN"):
            directorioAno = nombre[6:10]
            directorioMes = nombre[10:12]
            directorioDia = nombre[12:14]
            directorioAnexo = nombre[2:5]
        elif(nombre[0:1] == "g"):
            directorioAno = nombre[5:9]
            directorioMes = nombre[9:11]
            directorioDia = nombre[11:13]
            directorioAnexo = nombre[1:4]

        else:
            return
            
            
            
            #Se unen las variables para generar el directorio de destino
        
        destino = join(directorio_final,switch_laboratorio(directorioAnexo), directorioAno,switch_mes(directorioMes),directorioDia,directorioAnexo)
  
        #Valida si el directorio de destino existe, si no, se crea
        if ( not exists(destino) ):
            makedirs(destino)

       
        #Valida que el archivo no esté copiado ya, si el archivo ya está, no se copia
        if  (not exists(destino+"/"+nombre)):
            
            sftp.get(directorio_inicial+nombre , destino+"\\"+nombre)
            #copy ( origen, destino )
        #directorio inicial    
        print(nombre)
    #dirBase=directorio_inicial

    #ficheros = [ f for f in listdir(sftp.cwd("/var/spool/asterisk/monitor/")) if isfile(join(sftp.cwd("/var/spool/asterisk/monitor/"),f)) ]
    #for fich in ficheros:
        #if ( esFecha( fich ) ):
    for attr in directory_structure:
        if (attr.filename[0:3] == "OUT" and attr.filename[7:11] == año and attr.filename[11:13] == mes and attr.filename[13:15] == dia):
            moverFichero( attr.filename)
        elif (attr.filename[0:2] == "IN" and attr.filename[6:10] == año and attr.filename[10:12] == mes and attr.filename[12:14] == dia):
            moverFichero( attr.filename)
        elif (attr.filename[0:1] == "g" and attr.filename[5:9] == año and attr.filename[9:11] == mes and attr.filename[11:13] == dia):
            moverFichero( attr.filename)

print("Proceso terminado")